﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CbrDemo.Models
{
    public class FaultCase
    {
        public int ID { get; set; }
        public double[] Items { get; set; }
        public string Solution { get; set; }
    }

    public class FaultResponse
    {
        public IEnumerable<FaultCase> Cases { get; set; }
        public double Distance { get; set; }
    }
}
