﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CbrDemo.Models
{
    public class Measurement
    {
        public double Accel1Under { get; set; }
        public double Accel2Under { get; set; }
        public double Accel3Under { get; set; }

        public double Accel1Over { get; set; }
        public double Accel2Over { get; set; }
        public double Accel3Over { get; set; }
    }
}
