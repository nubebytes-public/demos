﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CbrDemo.Models
{
    public class CbrEnvironment
    {
        public string ContentRootPath { get; set; }

        public string StaticContentPath { get; set; }
    }
}
