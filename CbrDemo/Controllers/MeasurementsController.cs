﻿using CbrDemo.Models;
using CbrDemo.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CbrDemo.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MeasurementsController : ControllerBase
    {
        private DataService dataService;
        private CbrRetrievalService retrieval;

        public MeasurementsController(DataService dataService,CbrRetrievalService retrievalService)
        {
            this.dataService = dataService;
            this.retrieval = retrievalService;
        }


        [HttpGet]
        public IActionResult Get(int skip = 0, int take = 20)
        {
            return Ok(dataService.Get(skip, take));
        }

        [HttpPost("Fault/Similar")]
        public IActionResult GetSimilarCase([FromBody] FaultCase faultCase)
        {
            return Ok(retrieval.GetNearest(faultCase.Items));
        }

        [HttpPost("Fault/Similar/Solutions")]
        public IActionResult AddSolution([FromBody] FaultCase solution)
        {
            dataService.AddToFaultCases(solution);
            return NoContent();
        }

    }
}
