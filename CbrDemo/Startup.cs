using CbrDemo.Models;
using CbrDemo.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CbrDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            CurrentEnvironment = environment;
        }

        public IWebHostEnvironment CurrentEnvironment { get; }
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist/ClientApp";
            });
            services.AddSingleton<DataService>();
            services.AddTransient<CbrRetrievalService>();
            services.AddTransient((e) =>
            {
                return new CbrEnvironment
                {
                    ContentRootPath = CurrentEnvironment.ContentRootPath,
                    StaticContentPath = Path.Combine(CurrentEnvironment.ContentRootPath, "StaticContent")
                };
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            Func<HttpContext, bool> isApiRequest = (ctx) =>
            {
                return ctx.Request.Path.StartsWithSegments("/api");
            };

            app.MapWhen(isApiRequest, (app1) =>
            {
                app1.UseRouting();

                //BuildCors(app);
                app1.UseAuthentication();
                app1.UseAuthorization();

                app1.UseEndpoints(r =>
                {
                    r.MapControllers();
                });
            });
            app.MapWhen(ctx =>
            {
                return !isApiRequest(ctx);
            }, (app2) =>
            {
                app2.Use(async (c, next) =>
                {
                    bool disableCache = false;
                    if (!c.Request.Path.HasValue)
                        disableCache = true;
                    else
                    {
                        var path = c.Request.Path.Value.Split("/");
                        if (path.Any(e => "assets".Equals(e, StringComparison.OrdinalIgnoreCase)) || !path.LastOrDefault().Contains("."))
                            disableCache = true;
                    }
                    if (disableCache)
                    {
                        c.Response.Headers.Add("Cache-Control", "no-cache");
                        c.Response.Headers.Add("Pragma", "no-cache");
                    }
                    await next();
                });

                app2.UseSpa(spa =>
                {
                    // To learn more about options for serving an Angular SPA from ASP.NET Core,
                    // see https://go.microsoft.com/fwlink/?linkid=864501
                    spa.Options.StartupTimeout = TimeSpan.FromSeconds(90);
                    spa.Options.SourcePath = "ClientApp";
                    if (env.IsDevelopment())
                    {
                        spa.UseProxyToSpaDevelopmentServer("http://localhost:4070");
                        //spa.UseAngularCliServer(npmScript: "start");
                    }
                });
            });
        }
    }
}
