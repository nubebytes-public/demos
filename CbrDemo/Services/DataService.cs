﻿using CbrDemo.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CbrDemo.Services
{
    /// <summary>
    /// In real life this would fetch data from a db or another source
    /// </summary>
    public class DataService
    {
        private static object _lock = new object();
        private List<Measurement> measurements;
        private List<FaultCase> faultCases;
        private CbrEnvironment environment;

        public DataService(CbrEnvironment environment)
        {
            this.environment = environment;
            this.faultCases = LoadFaultcases();
        }

        public IEnumerable<Measurement> Get(int skip = 0, int take = 20)
        {
            lock (_lock)
            {
                if (measurements == null)
                    measurements = LoadDataset();
            }
            return measurements.Skip(skip).Take(take);
        }

        private List<Measurement> LoadDataset()
        {
            List<Measurement> dataset = new List<Measurement>();
            using StreamReader sr = new StreamReader(environment.StaticContentPath + Path.DirectorySeparatorChar + "dataset.csv");
            string line;
            do
            {
                line = sr.ReadLine();
                if (!string.IsNullOrEmpty(line))
                {
                    var measurement = ToMeasurement(line);
                    if(ValidateMeasurement(measurement))
                        dataset.Add(measurement);
                }
            } while (line != null);
            return dataset;
        }

        public void AddToFaultCases(FaultCase solution)
        {
            solution.ID = faultCases.Count;
            faultCases.Add(solution);
        }

        private bool ValidateMeasurement(Measurement measurement)
        {
            double limit = 2;
            return Math.Abs(measurement.Accel1Over) < limit && Math.Abs(measurement.Accel1Under) < limit &&
                Math.Abs(measurement.Accel2Over) < limit && Math.Abs(measurement.Accel2Under) < limit &&
                Math.Abs(measurement.Accel3Over) < limit && Math.Abs(measurement.Accel3Under) < limit;
        }

        private List<FaultCase> LoadFaultcases()
        {
            List<FaultCase> dataset = new List<FaultCase>();
            using StreamReader sr = new StreamReader(environment.StaticContentPath + Path.DirectorySeparatorChar + "errorDataset.csv");
            string line;
            int i = 0;
            do
            {
                line = sr.ReadLine();
                if (!string.IsNullOrEmpty(line))
                    dataset.Add(ToFaultCase(i, line));
                i++;
            } while (line != null);
            return dataset;
        }

        private Measurement ToMeasurement(string line)
        {
            var split = line.Split(",").Select(e => double.Parse(e, CultureInfo.InvariantCulture)).ToList();
            return new Measurement { 
                Accel1Under = split[0],
                Accel2Under = split[1],
                Accel3Under = split[2],
                Accel1Over = split[3],
                Accel2Over = split[4],
                Accel3Over = split[5],
            };
        }

        private FaultCase ToFaultCase(int id, string line)
        {
            var split = line.Split(",");
            string sol = split.Last();
            Array.Resize(ref split, split.Length - 1);
            return new FaultCase { ID = id, Items = split.Select(e => double.Parse(e, CultureInfo.InvariantCulture)).ToArray(), Solution = sol };
        }

        public List<FaultCase> GetFaultCases()
        {
            return faultCases;
        }


    }
}
