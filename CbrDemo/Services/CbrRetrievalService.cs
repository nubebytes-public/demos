﻿using CbrDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CbrDemo.Services
{
    /// <summary>
    /// Retrieves similar cases via k-NN algorithm
    /// </summary>
    public class CbrRetrievalService
    {

        private DataService datasource;

        public CbrRetrievalService(DataService datasource)
        {
            this.datasource = datasource;
        }

        public FaultResponse GetNearest(double[] item)
        {
            var trainData = datasource.GetFaultCases();
            var nearest = Classify(item, trainData.Select(e => e.Items).ToList(),  3);

            return new FaultResponse { Cases = nearest.Select(e =>  trainData[e.idx]), Distance = nearest.FirstOrDefault().dist };
        }


        public IEnumerable<IndexAndDistance> Classify(double[] unknown,
                List<double[]> trainData, int k)
        {
            int n = trainData.Count;
            IndexAndDistance[] info = new IndexAndDistance[n];
            for (int i = 0; i < n; ++i)
            {
                IndexAndDistance curr = new IndexAndDistance();
                double dist = Distance(unknown, trainData[i]);
                curr.idx = i;
                curr.dist = dist;
                info[i] = curr;
            }
            return info.OrderBy(e => e.dist).Take(k);
        }

        int Vote(IndexAndDistance[] info,
          List<double[]> trainData, int numClasses, int k)
        {
            int[] votes = new int[numClasses];  // One cell per class
            for (int i = 0; i < k; ++i)
            {       // Just first k
                int idx = info[i].idx;            // Which train item
                int c = (int)trainData[idx][2];   // Class in last cell
                ++votes[c];
            }
            int mostVotes = 0;
            int classWithMostVotes = 0;
            for (int j = 0; j < numClasses; ++j)
            {
                if (votes[j] > mostVotes)
                {
                    mostVotes = votes[j];
                    classWithMostVotes = j;
                }
            }
            return classWithMostVotes;
        }

        //For this sample app, we'll just use euclidean distance
        double Distance(double[] unknown,
          double[] data)
        {
            double sum = 0.0;
            for (int i = 0; i < unknown.Length; ++i)
                sum += (unknown[i] - data[i]) * (unknown[i] - data[i]);
            return Math.Sqrt(sum);
        }


    }

    public class IndexAndDistance : IComparable<IndexAndDistance>
    {
        public int idx;  // index of a training item
        public double dist;  // distance to unknown
        public int CompareTo(IndexAndDistance other)
        {
            if (this.dist < other.dist) return -1;
            else if (this.dist > other.dist) return +1;
            else return 0;
        }
    }

}
