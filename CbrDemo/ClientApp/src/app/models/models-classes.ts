
export class FaultCase {
  id?: number;
  items: number[];
  solution?: string;
}

export class Measurement {
  accel1Under: number;
  accel2Under: number;
  accel3Under: number;

  accel1Over: number;
  accel2Over: number;
  accel3Over: number;
}

export class GraphLine {
  name: string;
  series: Serie[];
}

export class Serie {
  value;
  name;
}

export class FaultResponse {
  cases: FaultCase[];
  distance: number;
}
