import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { max } from 'rxjs/operators';
import { FaultCase, FaultResponse } from '../models/models-classes';
import { MeasurementsService } from '../services/measurements.service';


@Component({
  selector: 'app-fault-modal',
  templateUrl: './fault-modal.component.html',
  styleUrls: ['./fault-modal.component.scss']
})
export class FaultModalComponent implements OnInit {

  @Input() res: FaultResponse;
  @Input() fault: FaultCase;

  yourSolution = "";

  constructor(public activeModal: NgbActiveModal, public service: MeasurementsService) { }

  ngOnInit(): void {
  }

  get quality() {
    //normalize res distance. Lower is better
    return Math.max(0,5 - this.res.distance);
  }

  accept() {
    if (this.yourSolution) {
      this.fault.solution = this.yourSolution;
      this.service.addSolution(this.fault).subscribe(() => this.activeModal.close());
    } else {
      this.activeModal.dismiss();
    }
  }

}
