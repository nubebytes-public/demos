import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FaultModalComponent } from './fault-modal/fault-modal.component';
import { FaultCase, GraphLine, Measurement } from './models/models-classes';
import { MeasurementsService } from './services/measurements.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'ClientApp';

  data1: GraphLine[];
  barData1;

  initialLoading = false;
  data2: GraphLine[];
  barData2;

  referenceLines1 = [{ name: "Min", value: -1 }, { name: "Max", value: 1 }];
  referenceLines2 = [{ name: "Min", value: -1 }, { name: "Max", value: 1.5 }];
  sub: Subscription;

  sampleFaultDataIx = 0;
  sampleFaultData: FaultCase[] = [{
    items: [-0.10974, -0.31435, -0.038086, -3.7199, -0.50, -2.4]
  },
  {
    items: [-0.10974, -0.31435, -0.038086, -3.7199, -0.50, -2.2]
    }, {
    items: [1.72635, 0.022666, 0.045817, 0.037942, 0.032747, 2.072439]
    }];

  yAxisTicks = [];

  constructor(public measurementsService: MeasurementsService, private datePipe: DatePipe, private ngbModal: NgbModal) {


    this.data1 = [
      {
        name: "Vibración superior 1",
        series: []
      },
      {
        name: "Vibración superior 2",
        series: []
      },
      {
        name: "Vibración superior 3",
        series: []
      }
    ];
    this.data2 = [
      {
        name: "Vibración inferior 1",
        series: []
      },
      {
        name: "Vibración inferior 2",
        series: []
      },
      {
        name: "Vibración inferior 3",
        series: []
      }
    ];
  }

  dateTickFormatting = (val) => {
    if (val instanceof Date) {
      return this.datePipe.transform(val, "HH:mm:ss");
    }
  }


  ngOnInit() {
    let jquery = (window as any).$;
    jquery("body").bootstrapMaterialDesign();
    this.measurementsService.getMeasurements().subscribe((items) => {
      this.initialLoading = true;
      for (let i = 0; i < items.length; i++) {
        this.toChartData(items[i], this.substractSeconds(new Date(), 5 * (items.length - i)));
      }
      this.sub = this.measurementsService.sub.subscribe((e) => {
        if (!this.simulating)
          this.toChartData(e, new Date());
      });
    });
  }

  private substractSeconds(date: Date, x: number) {
    return new Date(date.getTime() - 1000 * x);
  }

  toChartData = (item: Measurement, date: Date) => {
    this.barData1 = this.fillGraph(this.data1, item, "Over", date);
    this.data1 = [...this.data1];
    this.barData2 = this.fillGraph(this.data2, item, "Under", date);
    this.data2 = [...this.data2];
  }

  private fillGraph(graph: GraphLine[], item: Measurement, suffix: string, date: Date) {
    this.fillLine(graph, 0, item, suffix, date);
    this.fillLine(graph, 1, item, suffix, date);
    this.fillLine(graph, 2, item, suffix, date);
    const barData = [];
    for (let i = 0; i < graph.length; i++) {
      barData.push({ name: "Medida " + i, value: Math.abs(graph[i].series[graph[i].series.length - 1].value) });
    }
    return barData;
  }

  private fillLine(graph: GraphLine[], i: number, item: Measurement, suffix: string, date: Date) {
    if (graph[i].series.length >= 20)
      graph[i].series.shift();
    graph[i].series.push({ name: date, value: item["accel" + (i + 1) + suffix] });
  }

  ngOnDestroy() {
    this.sub?.unsubscribe();
  }

  simulating = false;

  simulateFailData() {
    if (this.sampleFaultDataIx > this.sampleFaultData.length - 1)
      return;
    this.simulating = true;
    this.toChartData(this.measurementsService.toMeasurement(this.sampleFaultData[this.sampleFaultDataIx]), new Date());

    this.measurementsService.getNearestFault(this.sampleFaultData[this.sampleFaultDataIx]).
      subscribe((e) => {
        const modal = this.ngbModal.open(FaultModalComponent, { backdrop: false, windowClass: 'modal-bot', size: 'xl' });
        modal.componentInstance.res = e;
        modal.componentInstance.fault = this.sampleFaultData[this.sampleFaultDataIx];
        modal.result.then(() => this.sampleFaultDataIx++).finally(() => this.simulating = false);
        console.log(e);
      });
  }


}
