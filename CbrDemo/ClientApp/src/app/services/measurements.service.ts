import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { FaultCase, FaultResponse, Measurement } from "../models/models-classes";
import { map, switchMap, tap } from 'rxjs/operators';
import { interval, Observable } from "rxjs";

@Injectable()
export class MeasurementsService {

  sub: Observable<Measurement>;

  private readonly take = 20;
  private i = this.take + 1;

  constructor(private http: HttpClient) {
    this.sub = interval(3000).pipe(switchMap(() => this.getMeasurements(this.i, 1)), tap(() => this.i++), map(e => e[0]));
  }

  getMeasurements(skip = 0, take = this.take) {
    return this.http.get<Measurement[]>(`/api/Measurements?skip=${skip}&take=${take}`)
  }

  getNearestFault(f: FaultCase) {
    return this.http.post<FaultResponse>(`/api/Measurements/Fault/Similar`, f);
  }

  addSolution(f: FaultCase) {
    return this.http.post(`/api/Measurements/Fault/Similar/Solutions`, f);
  }

  toMeasurement(f: FaultCase) : Measurement {
    return {
      accel1Under: f.items[0],
      accel2Under: f.items[1],
      accel3Under: f.items[2],
      accel1Over: f.items[3],
      accel2Over: f.items[4],
      accel3Over: f.items[5],
    };
  }

}
